package com.ecci.gallery

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val REQUEST_CODE_GALLERY = 1
    val REQUEST_CODE_CAMERA = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        galleryButton.setOnClickListener {
            pickerFromGallery()
        }
        cameraButton.setOnClickListener {
            selectFromCamera()
        }
        callButton.setOnClickListener {
            doCall()
        }
    }

    private fun doCall() {
        val call = Uri.parse("tel:3016464573")
        val intent = Intent(Intent.ACTION_DIAL, call)
        startActivity(intent)
    }

    private fun pickerFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE_GALLERY)
    }

    private fun selectFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CODE_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when(requestCode) {
            REQUEST_CODE_GALLERY -> {
                if(resultCode == RESULT_OK) {
                    val uri = intent?.data
                    imageView.setImageURI(uri)
                }
            }
            REQUEST_CODE_CAMERA -> {
                if(resultCode == RESULT_OK) {
                    val bitmap = intent?.extras?.get("data") as Bitmap
                    imageView.setImageBitmap(bitmap)
                }
            }
        }
    }
}